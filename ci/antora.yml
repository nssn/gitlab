# =====================================================================================================================
# @brief Run antora to generate a documentation web-site and publish it to GitLab Pages
# @link  https://nssn.gitlab.io/gitlab
#
# SPDX-FileCopyrightText:  2023 Nessan Fitzmaurice <nessan.fitzmaurice@me.com>
# SPDX-License-Identifier: MIT
# =====================================================================================================================

# Include this yml file in your .gitlab-ci.yml and set any inputs
spec:
  inputs:
    # Path to the directory with the Antora playbook file
    playbook_directory:
      default: $CI_PROJECT_DIR

    # The name of the playbook in that directory
    playbook_file:
      default: antora-playbook.yml

    # By default the jobs in this file run in the 'deploy' stage but you are free to change that.
    stage:
      default: deploy
---

pages:

  # By default these jobs run in the 'deploy' stage of the pipeline
  stage:
    $[[inputs.stage]]

  # The job is only ever run for commits to the main branch
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

  # Can kill this job
  interruptible: true

  # The Docker image that antora is based om
  image:
    name: node:16-alpine

  variables:

    # Store the UI bundle etc. between runs.
    ANTORA_CACHE_DIR: .cache/antora
    NODE_OPTIONS:     --max-old-space-size=4096

    # GitLab Pages has a default spot it wants the site to be built to
    SITE_DIR:  $CI_PROJECT_DIR/public

  # Make sure our Node.js requirements are up to date
  before_script:
    - cd $[[inputs.playbook_directory]]
    - npm ci

  # Run Antora & create the site in the spot GitLab Pages expects to find it
  script:
    - cd $[[inputs.playbook_directory]]
    - npx antora --fetch --redirect-facility=gitlab --to-dir $SITE_DIR --url $CI_PAGES_URL $[[inputs.playbook_file]]

  # Hang on the built site so GitLab can deploy it
  artifacts:
    paths:
      - $SITE_DIR
