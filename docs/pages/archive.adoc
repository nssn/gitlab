include::_common.adoc[]

:gl-job:      https://docs.gitlab.com/ee/ci/jobs/[GitLab CI pipeline job]
:gl-ci-file:  pass:q[`https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html[.gitlab-ci.yml]`]
:gl-package-registry: https://docs.gitlab.com/ee/user/packages/package_registry/[GitLab Package Registry]
:gl-release:  https://docs.gitlab.com/ee/user/project/releases/[GitLab Release]

= `archive.yml`

This defines a {gl-job} that generates a zip archive of named assets and attaches that zip file to a release for easy access.

== Usage

Include the job definition file in your own {gl-ci-file} file and set some input variables:
[source,yaml]
----
include:
  - remote: https://gitlab.com/nssn/gitlab/raw/main/ci/archive.yml  # <.>
    inputs:
      assets: CMakeLists.txt LICENSE include   # <.>
      archive: archive.zip                     # <.>
      stage: deploy                            # <.>
----
<.> Include the file which defines the `pages` job.
<.> The list of files/directories you want included in the archive. +
This must be set--there is no default value.
<.> The name you want for the archive. +
The default is `"$CI_PROJECT_NAME.zip"`
<.> The stage to run the job in--you need to be sure the stage is active. +
Default stage is `deploy`.

TIP: The job will run on commits to the default branch (typically `main`) but on no other branch.

For the sake of a concrete example imagine you (`OWNER`) are the GitLab owner of some `PROJECT` repo.

There are two scenarios - commits that push a tag (e.g. 2.3.0) to that repo, and all other commits.

In both cases, if the commits are on the main branch, this job will create an archive (`"PROJECT.zip"` by default) and upload that to the {gl-package-registry}.

You can retrieve the archive from there.
However, GitLab Package Registry URL's are not super friendly as they embed the numeric ID of the project.
That makes them obscure in scripts.

For that reason our jobs also always do a {gl-release} and attach the archive as an asset in that release.
The URL's for GitLab releases are based on the project name so are much friendlier.

To be concrete if you push the tag `'2.3.0'` to the repo then the archive will available at
[source]
----
https://gitlab.com/OWNER/PROJECT/-/releases/2.3.0/downloads/PROJECT.zip
----
Older versions will be available at 2.2.0, 2.1.0, ... as long they aren't cleared out of GitLab (so a long time!)

On the other hand, if you do any other sort of commit to the main branch then the archive will be available at
[source]
----
https://gitlab.com/OWNER/PROJECT/-/releases/current/downloads/PROJECT.zip
----

NOTE: In this case the previous "current" version of the archive is gone.
We delete and reuse 'current' every time!

You can use some other tag rather than `current` by setting the `default-tag` key in your inputs.
[source,yaml]
----
include:
  - remote: https://gitlab.com/nssn/gitlab/raw/main/ci/archive.yml  # <.>
    inputs:
      assets: CMakeLists.txt LICENSE include
      ...
      default-tag: untested
----
We could have used something dynamic for the tag but we don't want to eat registry space.
In any case, the intent is to have a readily available `current` version of the archive.
If you want to have other versions kept around push a tag!

=== TODO

It would be nice to have an inputs parameter, say `tags_only`, which could be set to `true` to disable the whole business of doing current releases.
If set to `true` then the only time an archive gets created would be when a tag is pushed to the repo.
It would default to `false`.
Unfortunately at the moment GitLab doesn't let you use a variable like that in a `rules:if:` statement.
